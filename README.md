# Erltris: An Erlang Tetris Implementation
This is a GUI implementation of the classic puzzle game Tetris.
It is written purely in Erlang using [ZX](https://zxq9.com/projects/zomp/).

## Playing Erltris
If you want to play Erltris and you have ZX already installed, check the platform instructions below.
If you do not yet have ZX installed, [install it](https://zxq9.com/projects/zomp/download.en.html) first and then follow the instructions below.

### Linux/BSD/OSX/etc
Either of:
- `zx run erltris`
- `zx run vapor` and then select Erltris from the list of available GUI programs.

### Windows
Windows users will find using the GUI interface for ZX (a desktop program called "Vapor") easiest.
The ZX installer for Windows is named "InstallVapor.msi" and will install Vapor along with a desktop icon and menu launcher.
Run Vapor and select Erltris from the list of available programs.
