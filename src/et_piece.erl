-module(et_piece).
-vsn("1.0.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([rand/0, new/1, flip/2, points/2, points/1, type/1, sides/1]).

-export_type([data/0]).

-record(p,
        {flip = 1 :: 1..4,
         type = i :: erltris:type()}).

-opaque data() :: #p{}.

rand() ->
    case rand:uniform(7) of
        1 -> new(i);
        2 -> new(o);
        3 -> new(t);
        4 -> new(s);
        5 -> new(z);
        6 -> new(j);
        7 -> new(l)
    end.


new(T) -> #p{type = T}.


flip(r, Piece = #p{flip = 4}) -> Piece#p{flip = 1};
flip(r, Piece = #p{flip = F}) -> Piece#p{flip = F + 1};
flip(l, Piece = #p{flip = 1}) -> Piece#p{flip = 4};
flip(l, Piece = #p{flip = F}) -> Piece#p{flip = F - 1}.


points(Piece, {LX, LY}) ->
    Offsets = points(Piece),
    Translate = fun({OX, OY}) -> {LX + OX, LY + OY} end,
    lists:map(Translate, Offsets).


points(#p{flip = F, type = T}) ->
    offset(T, F).

offset(i, 1) -> [{0, 2}, {1, 2}, {2, 2}, {3, 2}];
offset(i, 2) -> [{2, 3}, {2, 2}, {2, 1}, {2, 0}];
offset(i, 3) -> [{0, 1}, {1, 1}, {2, 1}, {3, 1}];
offset(i, 4) -> [{1, 3}, {1, 2}, {1, 1}, {1, 0}];
offset(o, _) -> [{1, 1}, {1, 2}, {2, 1}, {2, 2}];
offset(t, 1) -> [{0, 1}, {1, 1}, {2, 1}, {1, 2}];
offset(t, 2) -> [{1, 2}, {1, 1}, {1, 0}, {2, 1}];
offset(t, 3) -> [{0, 1}, {1, 1}, {2, 1}, {1, 0}];
offset(t, 4) -> [{1, 2}, {1, 1}, {1, 0}, {0, 1}];
offset(s, 1) -> [{0, 1}, {1, 1}, {1, 2}, {2, 2}];
offset(s, 2) -> [{1, 2}, {1, 1}, {2, 1}, {2, 0}];
offset(s, 3) -> [{0, 0}, {1, 0}, {1, 1}, {2, 1}];
offset(s, 4) -> [{0, 2}, {0, 1}, {1, 1}, {1, 0}];
offset(z, 1) -> [{0, 2}, {1, 2}, {1, 1}, {2, 1}];
offset(z, 2) -> [{1, 0}, {1, 1}, {2, 1}, {2, 2}];
offset(z, 3) -> [{0, 1}, {1, 1}, {1, 0}, {2, 0}];
offset(z, 4) -> [{0, 0}, {0, 1}, {1, 1}, {1, 2}];
offset(j, 1) -> [{0, 2}, {0, 1}, {1, 1}, {2, 1}];
offset(j, 2) -> [{1, 0}, {1, 1}, {1, 2}, {2, 2}];
offset(j, 3) -> [{0, 1}, {1, 1}, {2, 1}, {2, 0}];
offset(j, 4) -> [{0, 0}, {1, 0}, {1, 1}, {1, 2}];
offset(l, 1) -> [{0, 1}, {1, 1}, {2, 1}, {2, 2}];
offset(l, 2) -> [{1, 2}, {1, 1}, {1, 0}, {2, 0}];
offset(l, 3) -> [{0, 0}, {0, 1}, {1, 1}, {2, 1}];
offset(l, 4) -> [{0, 2}, {1, 2}, {1, 1}, {1, 0}].


type(#p{type = T}) -> T.


sides(#p{type = T, flip = F}) ->
    sides(T, F).


sides(i, 1) -> {0, 3, 2};
sides(i, 2) -> {2, 2, 3};
sides(i, 3) -> {0, 3, 1};
sides(i, 4) -> {1, 1, 3};
sides(o, _) -> {1, 2, 2};
sides(t, 1) -> {0, 2, 2};
sides(t, 2) -> {1, 2, 2};
sides(t, 3) -> {0, 2, 1};
sides(t, 4) -> {0, 1, 2};
sides(s, 1) -> {0, 2, 2};
sides(s, 2) -> {1, 2, 2};
sides(s, 3) -> {0, 2, 1};
sides(s, 4) -> {0, 1, 2};
sides(z, 1) -> {0 ,2, 2};
sides(z, 2) -> {1, 2, 2};
sides(z, 3) -> {0 ,2, 1};
sides(z, 4) -> {0, 1, 2};
sides(j, 1) -> {0, 2, 2};
sides(j, 2) -> {1, 2, 2};
sides(j, 3) -> {0, 2, 1};
sides(j, 4) -> {0, 1, 2};
sides(l, 1) -> {0, 2, 2};
sides(l, 2) -> {1, 2, 2};
sides(l, 3) -> {0, 2, 1};
sides(l, 4) -> {0, 1, 2}.
