%%% @doc
%%% Erltris GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(et_gui).
-vsn("1.0.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-export([show/1, game_over/0]).
-export([start_link/1]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {frame   = none          :: none | wxFrame:wxFrame(),
         board   = none          :: none | wxPanel:wxPanel(),
         well    = et_well:new() :: erltris:well(),
         next    = none          :: none | wxPanel:wxPanel(),
         next_p  = none          :: none | erltris:piece(),
         level   = none          :: none | wxTextCtrl:wxTextCtrl(),
         score   = none          :: none | wxTextCtrl:wxTextCtrl(),
         lines   = none          :: none | wxTextCtrl:wxTextCtrl(),
         count   = none          :: none | wxTextCtrl:wxTextCtrl(),
         sprites = sprites()     :: sprites()}).


-type sprites() :: #{x := wx:wxBitmap(),
                     i := wx:wxBitmap(),
                     o := wx:wxBitmap(),
                     t := wx:wxBitmap(),
                     s := wx:wxBitmap(),
                     z := wx:wxBitmap(),
                     j := wx:wxBitmap(),
                     l := wx:wxBitmap()}.


-define(gameNEW, 11).
-define(helpINS, 12).



%%% Interface functions

-spec show([Element]) -> ok
    when Element :: {well, erltris:well()}
                  | {vacated, [erltris:coord()]}
                  | {occupied, erltris:type(), [erltris:coord()]}
                  | {level, non_neg_integer()}
                  | {score, non_neg_integer()}
                  | {lines, non_neg_integer()}
                  | {count, non_neg_integer()}
                  | {next, erltris:piece()}.

show(Elements) ->
    wx_object:cast(?MODULE, {show, Elements}).


-spec game_over() -> ok.

game_over() ->
    wx_object:cast(?MODULE, game_over).



%%% Startup Functions

start_link(Title) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, Title, []).


init(Label) ->
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Label),

    MB = wxMenuBar:new(),
    Game = wxMenu:new([]),
    Help = wxMenu:new([]),
    _ = wxMenu:append(Game, ?gameNEW, "New Game"),
    _ = wxMenu:append(Game, ?wxID_EXIT, "&Quit"),
    _ = wxMenu:append(Help, ?helpINS, "&Instructions"),
    _ = wxMenu:append(Help, ?wxID_ABOUT, "About"),
    _ = wxMenuBar:append(MB, Game, "&Game"),
    _ = wxMenuBar:append(MB, Help, "&Help"),
    ok = wxFrame:setMenuBar(Frame, MB),

    Board = wxPanel:new(Frame),
    Well = et_well:new(),
    {W, H} = et_well:dimensions(Well),
    BoardPX = {W * 30, H * 30},
    ok = wxPanel:setSize(Board, BoardPX),

    StatOpts = [{value, "0"}, {style, ?wxTE_READONLY bor ?wxTE_RIGHT}],
    LevelBx = wxStaticBox:new(Frame, ?wxID_ANY, "Level"),
    LevelSz = wxStaticBoxSizer:new(LevelBx, ?wxHORIZONTAL),
    Level   = wxTextCtrl:new(LevelBx, ?wxID_ANY, StatOpts),
    ScoreBx = wxStaticBox:new(Frame, ?wxID_ANY, "Score"),
    ScoreSz = wxStaticBoxSizer:new(ScoreBx, ?wxHORIZONTAL),
    Score   = wxTextCtrl:new(ScoreBx, ?wxID_ANY, StatOpts),
    LinesBx = wxStaticBox:new(Frame, ?wxID_ANY, "Lines"),
    LinesSz = wxStaticBoxSizer:new(LinesBx, ?wxHORIZONTAL),
    Lines   = wxTextCtrl:new(LinesBx, ?wxID_ANY, StatOpts),
    CountBx = wxStaticBox:new(Frame, ?wxID_ANY, "Count"),
    CountSz = wxStaticBoxSizer:new(CountBx, ?wxHORIZONTAL),
    Count   = wxTextCtrl:new(CountBx, ?wxID_ANY, StatOpts),
    NextBx  = wxStaticBox:new(Frame, ?wxID_ANY, "Next"),
    NextSz  = wxStaticBoxSizer:new(NextBx, ?wxVERTICAL),
    Next = wxPanel:new(NextBx),
    NextPX = {6 * 30, 6 * 30},
    ok = wxPanel:setSize(Next, NextPX),
    _ = wxStaticBoxSizer:add(LevelSz, Level, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxStaticBoxSizer:add(ScoreSz, Score, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxStaticBoxSizer:add(LinesSz, Lines, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxStaticBoxSizer:add(CountSz, Count, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxStaticBoxSizer:add(NextSz, Next, [{flag, ?wxEXPAND}, {proportion, 1}]),
    ok = wxSizer:setMinSize(NextSz, {6 * 30, 6 * 30}),

    MainSz = wxBoxSizer:new(?wxHORIZONTAL),
    StatSz = wxBoxSizer:new(?wxVERTICAL),
    _ = wxSizer:add(MainSz, Board, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxSizer:add(MainSz, StatSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(StatSz, LevelSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(StatSz, ScoreSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(StatSz, LinesSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(StatSz, CountSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(StatSz, NextSz, [{flag, ?wxEXPAND}, {proportion, 1}]),
    
    ok = wxFrame:setSizer(Frame, MainSz),
    ok = wxSizer:layout(MainSz),

    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:connect(Frame, char_hook),
    ok = wxFrame:connect(Frame, command_menu_selected),
    ok = wxPanel:connect(Board, paint),
    ok = wxFrame:setClientSize(Frame, {(W * 30) + (6 * 30) + 30, H * 30}),
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),
    State = #s{frame = Frame, board = Board, level = Level,
               score = Score, lines = Lines, count = Count, next = Next},
    ok = redraw(State),
    NextWell = et_well:new(6, 6),
    ok = draw(NextWell, Next, State#s.sprites),
    {Frame, State}.

sprites() ->
    ImgDir = filename:join(zx_daemon:get_home(), "sprites"),
    PNG = [{type, ?wxBITMAP_TYPE_PNG}],
    #{x => wxBitmap:new(filename:join(ImgDir, "x.png"), PNG),
      i => wxBitmap:new(filename:join(ImgDir, "i.png"), PNG),
      o => wxBitmap:new(filename:join(ImgDir, "o.png"), PNG),
      t => wxBitmap:new(filename:join(ImgDir, "t.png"), PNG),
      s => wxBitmap:new(filename:join(ImgDir, "s.png"), PNG),
      z => wxBitmap:new(filename:join(ImgDir, "z.png"), PNG),
      j => wxBitmap:new(filename:join(ImgDir, "j.png"), PNG),
      l => wxBitmap:new(filename:join(ImgDir, "l.png"), PNG)}.


%%% wx_object

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast({show, Elements}, State) ->
    NewState = lists:foldl(fun do_show/2, State, Elements),
    {noreply, NewState};
handle_cast(game_over, State) ->
    ok = do_game_over(State),
    {noreply, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_event(#wx{event = #wxKey{keyCode = Code}}, State = #s{frame = Frame}) ->
    ok =
        case Code of
             32 -> et_con:move(bottom);
             80 -> do_pause(Frame);
             88 -> et_con:rotate(l);
             90 -> et_con:rotate(r);
            310 -> do_pause(Frame);
            314 -> et_con:move(l);
            315 -> et_con:rotate(r);
            316 -> et_con:move(r);
            317 -> et_con:move(d);
              _ -> ok
        end,
    {noreply, State};
handle_event(#wx{id = ID, event = #wxCommand{type = command_menu_selected}}, State) ->
    NewState =
        case ID of
            ?gameNEW    -> new_game(State);
            ?helpINS    -> show_instructions(State);
            ?wxID_EXIT  -> close(State);
            ?wxID_ABOUT -> show_about(State)
        end,
    {noreply, NewState};
handle_event(#wx{event = #wxPaint{}}, State) ->
    ok = redraw(State),
    {noreply, State};
handle_event(#wx{event = #wxClose{}}, State) ->
    NewState = close(State),
    {noreply, NewState};
handle_event(Event, State) ->
    ok = tell(info, "Unexpected event ~tp State: ~tp~n", [Event, State]),
    {noreply, State}.


code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().



%%% Doer Functions

new_game(State) ->
    Well = et_well:new(),
    Fresh =
        [{well,  Well},
         {level, 1},
         {score, 0},
         {lines, 0},
         {count, 0}],
    ok = et_con:new_game(),
    lists:foldl(fun do_show/2, State#s{well = Well}, Fresh).
    


do_show({well, Well}, State = #s{board = Board, sprites = Sprites}) ->
    ok = draw(Well, Board, Sprites),
    State#s{well = Well};
do_show({level, Number}, State = #s{level = Level}) ->
    ok = wxTextCtrl:changeValue(Level, integer_to_list(Number)),
    State;
do_show({score, Number}, State = #s{score = Score}) ->
    ok = wxTextCtrl:changeValue(Score, integer_to_list(Number)),
    State;
do_show({lines, Number}, State = #s{lines = Lines}) ->
    ok = wxTextCtrl:changeValue(Lines, integer_to_list(Number)),
    State;
do_show({count, Number}, State = #s{count = Count}) ->
    ok = wxTextCtrl:changeValue(Count, integer_to_list(Number)),
    State;
do_show({next, Piece}, State = #s{next = Next, sprites = Sprites}) ->
    ok = draw_next(Piece, Next, Sprites),
    State#s{next_p = Piece};
do_show({occupied, Type, Points},
        State = #s{well = Well, board = Board, sprites = Sprites}) ->
    NewWell = fill(Type, Points, Well),
    ok = draw(Points, Board, Type, Sprites),
    State#s{well = NewWell};
do_show({vacated, Points},
        State = #s{well = Well, board = Board, sprites = Sprites}) ->
    NewWell = fill(x, Points, Well),
    ok = draw(Points, Board, x, Sprites),
    State#s{well = NewWell}.


fill(Type, Points, Well) ->
    Store = fun({X, Y}, W) -> et_well:store(W, Type, X, Y) end,
    lists:foldl(Store, Well, Points).


draw(Points, Board, Type, Sprites) ->
    DC = wxClientDC:new(Board),
    Sprite = maps:get(Type, Sprites),
    Draw =
        fun({X, Y}) ->
            Coord = {(X - 1) * 30, (Y - 1) * 30},
            wxClientDC:drawBitmap(DC, Sprite, Coord)
        end,
    ok = lists:foreach(Draw, Points),
    wxClientDC:destroy(DC).


draw(Well, Board, Sprites) ->
    {W, H} = et_well:dimensions(Well),
    DC = wxClientDC:new(Board),
    ok = draw(1, 1, W, H, DC, Well, Sprites),
    wxClientDC:destroy(DC).

draw(W, H, W, H, DC, Well, Sprites) ->
    paint(W, H, DC, Well, Sprites);
draw(W, Y, W, H, DC, Well, Sprites) ->
    ok = paint(W, Y, DC, Well, Sprites),
    draw(1, Y + 1, W, H, DC, Well, Sprites);
draw(X, Y, W, H, DC, Well, Sprites) ->
    ok = paint(X, Y, DC, Well, Sprites),
    draw(X + 1, Y, W, H, DC, Well, Sprites).

paint(X, Y, DC, Well, Sprites) ->
    T = et_well:fetch(Well, X, Y),
    Image = maps:get(T, Sprites),
    Coord = {(X - 1) * 30, (Y - 1) * 30},
    wxClientDC:drawBitmap(DC, Image, Coord).


redraw(#s{next_p = Piece, next  = Next,
          well   = Well,  board = Board,
          sprites = Sprites}) ->
    ok = draw_next(Piece, Next, Sprites),
    draw(Well, Board, Sprites).


draw_next(none, Next, Sprites) ->
    Well = et_well:new(6, 6),
    draw(Well, Next, Sprites);
draw_next(Piece, Next, Sprites) ->
    Well = et_well:new(6, 6),
    Type = et_piece:type(Piece),
    Points = et_piece:points(Piece, {2, 2}),
    NewWell = fill(Type, Points, Well),
    draw(NewWell, Next, Sprites).


do_pause(Frame) ->
    ok = et_con:pause(),
    Options = [{caption, "PAUSE"}],
    Message = "GAME PAUSED",
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    wxMessageDialog:destroy(Dialog),
    et_con:unpause().


do_game_over(#s{frame = Frame}) ->
    Options = [{caption, "GAME OVER"}],
    Message = "GAME OVER",
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    wxMessageDialog:destroy(Dialog).
    


show_about(State = #s{frame = Frame}) ->
    ok = et_con:pause(),
    Version = proplists:get_value(vsn, module_info(attributes)),
    Format =
        "Erltris v~s\n"
        "A tetris clone, in Erlang\n"
        "Author: (c) 2020 Craig Everett <zxq9@zxq9.com>\n"
        "License: MIT (open source; free for any use)\n"
        "Repo URL: https://gitlab.com/zxq9/erltris\n"
        "Zomp package ID: otpr-erltris",
    Message = io_lib:format(Format, [Version]),
    Options = [{caption, "About"}, {style, ?wxICON_INFORMATION}],
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    ok = wxMessageDialog:destroy(Dialog),
    ok = et_con:unpause(),
    State.


show_instructions(State = #s{frame = Frame}) ->
    ok = et_con:pause(),
    Message =
        "Movement: Arrow keys\n"
        "Rotation: <Z> and <X>\n"
        "Hard Drop: <SPACE>",
    Options = [{caption, "Instructions"}, {style, ?wxICON_INFORMATION}],
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    _ = wxMessageDialog:showModal(Dialog),
    ok = wxMessageDialog:destroy(Dialog),
    ok = et_con:unpause(),
    State.


close(State = #s{frame = Frame}) ->
    ok = et_con:stop(),
    ok = wxWindow:destroy(Frame),
    State.
