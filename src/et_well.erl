-module(et_well).
-vsn("1.0.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([new/0, new/2,
         dimensions/1, height/1, width/1,
         fetch/3, store/4, complete/1, collapse/2]).

-export_type([playfield/0]).


-opaque playfield() :: tuple().


new() ->
    new(10, 20).


new(W, H) ->
    erlang:make_tuple(H, row(W)).


row(W) ->
    erlang:make_tuple(W, x).


dimensions(Well) ->
    H = size(Well),
    W = size(element(1, Well)),
    {W, H}.


height(Well) ->
    size(Well).


width(Well) ->
    size(element(1, Well)).


fetch(Well, X, Y) ->
    element(X, element(Y, Well)).


store(Well, Value, X, Y) ->
    setelement(Y, Well, setelement(X, element(Y, Well), Value)).


complete(Well) ->
    {W, H} = dimensions(Well),
    complete(H, W, Well, []).

complete(Y, W, Well, Lines) when Y >= 1 ->
    case line_complete(W, element(Y, Well)) of
        true  -> complete(Y - 1, W, Well, [Y | Lines]);
        false -> complete(Y - 1, W, Well, Lines)
    end;
complete(_, _, _, Lines) ->
    Lines.

line_complete(X, Line) when X >= 1 ->
    case element(X, Line) of
        x -> false;
        _ -> line_complete(X - 1, Line)
    end;
line_complete(_, _) ->
    true.


collapse(Well, Lines) ->
    Blank = row(width(Well)),
    Crunch =
        fun(L, {W, Count}) ->
            Crunched = erlang:insert_element(1, erlang:delete_element(L, W), Blank),
            {Crunched, Count + 1}
        end,
    lists:foldl(Crunch, {Well, 0}, Lines).
