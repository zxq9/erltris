%%% @doc
%%% Erltris Controller
%%%
%%% This process is a in charge of maintaining the program's state.
%%% @end

-module(et_con).
-vsn("1.0.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(gen_server).
% UI control
-export([new_game/0]).
% Game control
-export([rotate/1, move/1, pause/0, unpause/0]).
% System control
-export([start_link/0, stop/0]).
% gen_server
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {window  = none          :: none | wx:wx_object(),
         well    = et_well:new() :: erltris:well(),
         loc     = none          :: none | erltris:coord(),
         piece   = none          :: none | erltris:piece(),
         next    = none          :: none | erltris:piece(),
         level   = 1             :: non_neg_integer(),
         score   = 0             :: non_neg_integer(),
         lines   = 0             :: non_neg_integer(),
         count   = 0             :: non_neg_integer(),
         dropped = 0             :: non_neg_integer(),
         delay   = 1000          :: pos_integer(),
         timer   = none          :: none | reference() | {paused, pos_integer()}}).

-type state() :: #s{}.


%%% UI Control

-spec new_game() -> ok.

new_game() ->
    gen_server:cast(?MODULE, new_game).


%%% Game control


-spec rotate(Direction) -> ok
    when Direction :: l | r.

rotate(Direction) ->
    gen_server:cast(?MODULE, {rotate, Direction}).


-spec move(Direction) -> ok
    when Direction :: l | r | d | bottom.

move(Direction) ->
    gen_server:cast(?MODULE, {move, Direction}).


-spec pause() -> ok.

pause() ->
    gen_server:cast(?MODULE, pause).


-spec unpause() -> ok.

unpause() ->
    gen_server:cast(?MODULE, unpause).


%%% System control

-spec stop() -> ok.

stop() ->
    gen_server:cast(?MODULE, stop).



%%% Startup Functions


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by et_sup.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    ok = log(info, "Starting"),
    Window = et_gui:start_link("Erltris"),
    ok = log(info, "Window: ~p", [Window]),
    State = #s{window = Window},
    {ok, State}.




%%% gen_server


handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast(new_game, State) ->
    NewState = do_new_game(State),
    {noreply, NewState};
handle_cast({rotate, Direction}, State) ->
    NewState = do_rotate(Direction, State),
    {noreply, NewState};
handle_cast({move, Direction}, State) ->
    NewState = do_move(Direction, State),
    {noreply, NewState};
handle_cast(pause, State) ->
    NewState = do_pause(State),
    {noreply, NewState};
handle_cast(unpause, State) ->
    NewState = do_unpause(State),
    {noreply, NewState};
handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(tic, State) ->
    NewState = do_tic(State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    zx:stop().



%%% Doer functions

do_new_game(#s{timer = T}) ->
    ok =
        case T == none of
            false ->
                _ = erlang:cancel_timer(T),
                ok;
            true ->
                ok
        end,
    NewT = erlang:send_after(1000, self(), tic),
    Next = et_piece:rand(),
    Loc = {4, 1},
    #s{next = Next, loc = Loc, timer = NewT}.


point_diff(OldPiece, OldLoc, Occupied) ->
    Footprint = et_piece:points(OldPiece, OldLoc),
    lists:subtract(Footprint, Occupied).


do_tic(State = #s{well  = Well,
                  piece = none,  next = Piece,
                  delay = Delay, count = Count}) ->
    T = erlang:send_after(Delay, self(), tic),
    Loc = start_loc(Well),
    Type = et_piece:type(Piece),
    Points = et_piece:points(Piece, Loc),
    NewCount = Count + 1,
    Next = et_piece:rand(),
    ok = et_gui:show([{count, NewCount}, {occupied, Type, Points}, {next, Next}]),
    NewState = State#s{loc = Loc, piece = Piece, next = Next,
                       count = NewCount, timer = T},
    case collision(p, Points, Well) of
        none ->
            NewState;
        stuck ->
            _ = erlang:cancel_timer(T),
            ok = timer:sleep(1000),
            ok = et_gui:game_over(),
            NewState#s{timer = none}
    end;
do_tic(State) ->
    NextState = #s{delay = Delay} = make_move(d, State),
    T = erlang:send_after(Delay, self(), tic),
    NextState#s{timer = T}.


start_loc(Well) ->
    W = et_well:width(Well),
    {(W div 2) - 1, 1}.


do_rotate(_, State = #s{piece = none}) ->
    State;
do_rotate(Direction, State = #s{well = Well, loc = Loc, piece = Piece}) ->
    case eval_rotation(Direction, Piece, Loc, Well) of
        {ok, Vacated, Occupied, NewPiece} ->
            Type = et_piece:type(Piece),
            ok = et_gui:show([{vacated, Vacated}, {occupied, Type, Occupied}]),
            State#s{piece = NewPiece};
        stuck ->
            State
    end.


eval_rotation(Direction, Piece, Loc, Well) ->
    NewPiece = et_piece:flip(Direction, Piece),
    Points = et_piece:points(NewPiece, Loc),
    {W, H} = et_well:dimensions(Well),
    Open =
        fun({X, Y}) ->
            case (X >= 1) andalso (X =< W) andalso (Y >= 1) andalso (Y =< H) of
                true  -> et_well:fetch(Well, X, Y) == x;
                false -> false
            end
        end,
    case lists:all(Open, Points) of
        true ->
            Vacated = point_diff(Piece, Loc, Points),
            {ok, Vacated, Points, NewPiece};
        false ->
            stuck
    end.


do_move(_, State = #s{piece = none}) ->
    State;
do_move(_, State = #s{timer = none}) ->
    State;
do_move(Direction, State = #s{timer = T, delay = Delay})
        when Direction == d; Direction == bottom ->
    _ = erlang:cancel_timer(T),
    NewT = erlang:send_after(Delay, self(), tic),
    NewState = make_move(Direction, State),
    NewState#s{timer = NewT};
do_move(Direction, State) ->
    make_move(Direction, State).


make_move(bottom, State = #s{well = Well, loc = {_, Y}, level = Level}) ->
    NewState = #s{score = Score} = hard_drop(State),
    H = et_well:height(Well),
    NewScore = Score + (Level * (H - Y)),
    ok = et_gui:show([{score, NewScore}]),
    NewState#s{score = NewScore};
make_move(Direction, State = #s{well = Well, loc = OldLoc, piece = Piece}) ->
    case blocked(Direction, Well, OldLoc, Piece) of
        {ok, NewLoc} -> make_move(Direction, NewLoc, State);
        stuck        -> State;
        locked       -> locked(et_piece:points(Piece, OldLoc), State)
    end.


hard_drop(State) ->
    case make_move(d, State) of
        NewState = #s{piece = none} -> NewState;
        NewState                    -> hard_drop(NewState)
    end.


make_move(Direction, NewLoc, State = #s{well = Well, loc = OldLoc, piece = Piece}) ->
    Occupied = et_piece:points(Piece, NewLoc),
    case collision(Direction, Occupied, Well) of
        none ->
            Vacated = point_diff(Piece, OldLoc, Occupied),
            Type = et_piece:type(Piece),
            ok = et_gui:show([{vacated, Vacated}, {occupied, Type, Occupied}]),
            State#s{loc = NewLoc};
        stuck ->
            State;
        locked ->
            locked(et_piece:points(Piece, OldLoc), State)
    end.


blocked(Direction, Well, {X, Y}, Piece) ->
    {W, H} = et_well:dimensions(Well),
    {L, R, B} = et_piece:sides(Piece),
    MaxR = W - R,
    MaxL = 1 - L,
    MaxB = H - B,
    case Direction of
        r when X  < MaxR -> {ok, {X + 1, Y}};
        l when X >  MaxL -> {ok, {X - 1, Y}};
        d when Y  < MaxB -> {ok, {X, Y + 1}};
        r when X >= MaxR -> stuck;
        l when X =< MaxL -> stuck;
        d when Y >= MaxB -> locked
    end.


collision(Direction, [{X, Y} | Rest], Well) ->
    case {et_well:fetch(Well, X, Y), Direction} of
        {x, _} -> collision(Direction, Rest, Well);
        {_, d} -> locked;
        {_, _} -> stuck
    end;
collision(_, [], _) ->
    none.


locked(Occupied,
       State = #s{well = Well, piece = Piece, delay = Delay,
                  level = Level, score = Score, lines = Lines}) ->
    Type = et_piece:type(Piece),
    Place = fun({X, Y}, W) -> et_well:store(W, Type, X, Y) end,
    Locked = lists:foldl(Place, Well, Occupied),
    {NewWell, NewDelay, NewLevel, NewScore, NewLines} =
        case et_well:complete(Locked) of
            [] ->
                {Locked, Delay, Level, Score, Lines};
            Completed ->
                {Collapsed, LineCount} = et_well:collapse(Locked, Completed),
                NextLines = Lines + LineCount,
                NextScore = Score + pow(LineCount * Level, LineCount),
                NextLevel = (NextLines div 10) + 1,
                NextDelay =
                    case Level == NextLevel of
                        true  -> Delay;
                        false -> Delay - (Delay div 10)
                    end,
                Updated =
                    [{well, Collapsed},
                     {level, NextLevel},
                     {score, NextScore},
                     {lines, NextLines}],
                ok = et_gui:show(Updated),
                {Collapsed, NextDelay, NextLevel, NextScore, NextLines}
        end,
    State#s{well  = NewWell,  delay = NewDelay, piece = none,
            level = NewLevel, score = NewScore, lines = NewLines}.


pow(N, Exp) ->
    pow(N, 1, Exp).

pow(N, A, Exp) when Exp > 0 ->
    pow(N, N * A, Exp - 1);
pow(_, A, _) ->
    A.


do_pause(State = #s{timer = T}) when is_reference(T) ->
    Left = erlang:cancel_timer(T),
    State#s{timer = {paused, Left}};
do_pause(State) ->
    State.


do_unpause(State = #s{timer = {paused, Left}}) ->
    T = erlang:send_after(Left, self(), tic),
    State#s{timer = T};
do_unpause(State) ->
    State.
