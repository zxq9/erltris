{application,erltris,
             [{description,"A tetris clone in Erlang"},
              {registered,[]},
              {included_applications,[]},
              {applications,[stdlib,kernel]},
              {vsn,"1.0.4"},
              {modules,[erltris,et_con,et_gui,et_piece,et_sup,et_well]},
              {mod,{erltris,[]}}]}.
